# Tramoji

Tramoji is a custom-made emoji pack featuring Traboone Social assets.

## Copyright

(C) 2020 Sandia Mesa Animation Studios
Tramoji is licensed under the [CC-BY-4.0](LICENSE) license.
